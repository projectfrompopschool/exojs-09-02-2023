class Eleves {
    constructor(nom, prenom, age) {
      this.nom = nom;
      this.prenom = prenom;
      this.age = age;
    }

    getNom(){
        return this.nom;
    }

    setNom(nom){
        this.nom = nom;
    }

    getPrenom(){
        return this.prenom;
    }

    setPrenom(prenom){
        this.prenom = prenom;
    }

    getAge(){
        return this.age;
    }

    setAge(age){
        this.age = age;
    }

}    
