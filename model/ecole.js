class Ecole {
    constructor(nom, eleves = []) {
        this.id = UUID();
        this.nom = nom;
        this.eleves = eleves;
    }

    getID() {
        return this.id;
    }

    getNom() {
        return this.nom;
    }

    setNom(nom) {
        this.nom = nom;
    }

    addEleve(eleve) {
        this.eleves.push(eleve);
    }

    listEleve() {
        return this.eleves;
    }

    removeEleve(eleve) {
        this.eleves = this.eleves.filter(function (e) {
            return eleve !== e;
        });
    }

    nombreEleve(){
        return this.eleves.length;

    }

}