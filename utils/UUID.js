const chiffreMax = 57;
const chiffreMin = 48;
const lettreMax = 102;
const lettreMin = 97;

function UUID() {
var uuid = "";

for (let i = 0; i < 4; i++) {
    for (let y = 0; y < 6; y++) {
        let verif = Math.round(Math.random());
        if (verif == 0) {
            uuid += String.fromCharCode(Math.floor(Math.random() * (chiffreMax - chiffreMin) + chiffreMin))
        } else {
            uuid += String.fromCharCode(Math.floor(Math.random() * (lettreMax - lettreMin) + lettreMin))
        }
    }
    uuid += "-";
}
return uuid.substring(0, uuid.length - 1);
}