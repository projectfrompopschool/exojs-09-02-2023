function createCardEcole(ecole){
    const nodeTable = document.createElement("table");
    nodeTable.setAttribute("class", "table");

    const nodeH1 = document.createElement("h1");
    document.getElementById("divOne").appendChild(nodeH1);

    document.getElementById("divOne").appendChild(nodeTable);

    const textnodeNomEcole = document.createTextNode(ecole.getNom());
    nodeH1.appendChild(textnodeNomEcole);

    const nodeThead = document.createElement("thead");
    nodeTable.appendChild(nodeThead);
    const nodeTr = document.createElement("tr");
    nodeThead.appendChild(nodeTr);
    const nodeTh = document.createElement("th");
    nodeTr.appendChild(nodeTh);
    const textnodeTitreNom = document.createTextNode("Nom");
    nodeTh.appendChild(textnodeTitreNom);
    const nodeTh1 = document.createElement("th");
    nodeTr.appendChild(nodeTh1);
    const textnodeTitrePrenom = document.createTextNode("Prenom");
    nodeTh1.appendChild(textnodeTitrePrenom);
    const nodeTh2 = document.createElement("th");
    nodeTr.appendChild(nodeTh2);
    const textnodeTitreAge = document.createTextNode("Age");
    nodeTh2.appendChild(textnodeTitreAge);

    const nodeTbody = document.createElement("tbody");
    nodeTable.appendChild(nodeTbody);
    nodeTbody.setAttribute("id", "NouveauEleve");
}