function createNewEleve(ecole){

    let inputnomNouvelleEleve = document.getElementById("nomNouvelleEleve").value;
    let inputprenomNouvelleEleve = document.getElementById("prenomNouvelleEleve").value;
    let inputageNouvelleEleve = document.getElementById("ageNouvelleEleve").value;
    let newEleve = new Eleves(inputnomNouvelleEleve, inputprenomNouvelleEleve, inputageNouvelleEleve);
    ecole.addEleve(newEleve);
    console.log(ecole);

    const nodecolone = document.createElement("tr");

    const nodenom = document.createElement("td");
    const textnode = document.createTextNode(inputnomNouvelleEleve);

    nodecolone.appendChild(nodenom);
    nodenom.appendChild(textnode);
    document.getElementById("NouveauEleve").appendChild(nodecolone);

    const nodeprenom = document.createElement("td");
    const textnodeprenom = document.createTextNode(inputprenomNouvelleEleve);

    nodecolone.appendChild(nodeprenom);
    nodeprenom.appendChild(textnodeprenom);

    const nodeage = document.createElement("td");
    const textnodeage = document.createTextNode(inputageNouvelleEleve);
    
    nodecolone.appendChild(nodeage);
    nodeage.appendChild(textnodeage);

};