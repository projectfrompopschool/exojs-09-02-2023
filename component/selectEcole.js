function createSelectEcole(ecole) {
    const nodeSelect = document.getElementById("selectOne");

    const nodeOption = document.createElement("option");

    const textnodeNomEcole = document.createTextNode(ecole.getNom());

    nodeOption.appendChild(textnodeNomEcole);

    nodeOption.setAttribute("value",ecole.getID());

    nodeSelect.appendChild(nodeOption);
}